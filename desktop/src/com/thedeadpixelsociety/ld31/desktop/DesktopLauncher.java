package com.thedeadpixelsociety.ld31.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.thedeadpixelsociety.ld31.LD31Game;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Ludum Dare 31 - Entire Game On One Screen";
		config.width = 800;
		config.height = 600;
		config.resizable = false;
		new LwjglApplication(new LD31Game(), config);
	}
}
