package com.thedeadpixelsociety.ld31.util;

import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.ObjectMap;

public final class GameServices implements Disposable {
    private final ObjectMap<Class, Object> servicesMap = new ObjectMap<Class, Object>();

    public void add(final Object service) {
        if (service != null) {
            servicesMap.put(service.getClass(), service);
        }
    }

    public void clear() {
        dispose();
        servicesMap.clear();
    }

    @Override
    public void dispose() {
        for (final ObjectMap.Entry<Class, Object> entry : servicesMap) {
            final Object service = entry.value;
            if (service instanceof Disposable) {
                Disposable disposable = (Disposable) service;
                disposable.dispose();
            }
        }
    }

    @SuppressWarnings("unchecked")
    public <T> T get(final Class<T> serviceClass) {
        return (T) servicesMap.get(serviceClass, null);
    }
}
