package com.thedeadpixelsociety.ld31;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.thedeadpixelsociety.ld31.scenes.GameScene;
import com.thedeadpixelsociety.ld31.scenes.SceneService;
import com.thedeadpixelsociety.ld31.util.GameServices;

public class LD31Game extends ApplicationAdapter {
    private static final float DT = .01f;
    private static final float MAX_DT = .16f;
    private final GameServices gameServices = new GameServices();
    private final SceneService sceneService = new SceneService();
    private float accumulator;
    private float totalTime;

    @Override
    public void create() {
        gameServices.add(new SpriteBatch());
        gameServices.add(sceneService);

        sceneService.add(new GameScene(gameServices));
    }

    @Override
    public void resize(final int width, final int height) {
        sceneService.resize(width, height);
    }

    @Override
    public void render() {
        float dt = Math.min(MAX_DT, Gdx.graphics.getDeltaTime());

        accumulator += dt;

        while (accumulator >= DT) {
            update(totalTime, DT);

            accumulator -= DT;
            totalTime += DT;
        }

        draw();
    }

    @Override
    public void pause() {
        sceneService.pause();
    }

    @Override
    public void resume() {
        sceneService.resume();
    }

    @Override
    public void dispose() {
        sceneService.dispose();
    }

    private void update(final float tt, final float dt) {
        sceneService.update(tt, dt);
    }

    private void draw() {
        Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        sceneService.draw();
    }
}
