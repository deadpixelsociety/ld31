package com.thedeadpixelsociety.ld31.scenes;

import com.badlogic.gdx.utils.Disposable;

import java.util.Stack;

public class SceneService implements Disposable {
    private final Stack<Scene> scenes = new Stack<Scene>();
    private int height;
    private int width;

    public void add(final Scene scene) {
        if (scene != null) {
            scene.added();
            scene.resize(width, height);

            scenes.push(scene);
        }
    }

    @Override
    public void dispose() {
        for (final Scene scene : scenes) {
            if (scene != null) {
                scene.dispose();
            }
        }
    }

    public void draw() {
        for (final Scene scene : scenes) {
            if (scene != null) {
                scene.draw();
            }
        }
    }

    public void pause() {
        for (final Scene scene : scenes) {
            if (scene != null) {
                scene.pause();
            }
        }
    }

    public void resize(final int width, final int height) {
        this.width = width;
        this.height = height;
    }

    public void resume() {
        for (final Scene scene : scenes) {
            if (scene != null) {
                scene.resume();
            }
        }
    }

    public void update(final float tt, final float dt) {
        boolean covered = false;
        for (int i = scenes.size() - 1; i >= 0; i--) {
            final Scene scene = scenes.get(i);
            if (scene != null) {
                scene.update(tt, dt);

                if (covered) {
                    remove(scene);
                }

                if (!scene.isOverlay()) {
                    covered = true;
                }
            }
        }
    }

    public void remove(final Scene scene) {
        if (scene != null) {
            scene.removed();
            scene.dispose();

            scenes.remove(scene);
        }
    }
}
