package com.thedeadpixelsociety.ld31.scenes;

import com.badlogic.gdx.utils.Disposable;
import com.thedeadpixelsociety.ld31.util.GameServices;

public interface Scene extends Disposable {
    void added();

    void draw();

    boolean isOverlay();

    void pause();

    void removed();

    void resize(final int width, final int height);

    void resume();

    void update(final float tt, final float dt);
}
