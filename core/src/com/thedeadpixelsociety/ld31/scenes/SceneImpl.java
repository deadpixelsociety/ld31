package com.thedeadpixelsociety.ld31.scenes;

import com.thedeadpixelsociety.ld31.util.GameServices;

public abstract class SceneImpl implements Scene {
    private final GameServices gameServices;

    protected SceneImpl(final GameServices gameServices) {
        this.gameServices = gameServices;
    }

    @Override
    public void added() {

    }

    @Override
    public void draw() {

    }

    @Override
    public boolean isOverlay() {
        return false;
    }

    @Override
    public void pause() {

    }

    @Override
    public void removed() {

    }

    @Override
    public void resize(final int width, final int height) {

    }

    @Override
    public void resume() {

    }

    @Override
    public void update(final float tt, final float dt) {

    }

    @Override
    public void dispose() {

    }

    public GameServices getGameServices() {
        return gameServices;
    }
}
